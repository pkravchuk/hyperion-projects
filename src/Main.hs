module Main where

import           Config                         (config)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.Scalars3d.IsingSigEpsTest2020
import qualified Projects.Fermions3d.FourFermions3dTest2020
import qualified Projects.Fermions3d.GNYTest2020
import qualified Projects.Fermions3d.GNYSigPsiEpsTest2020
import qualified Projects.Fermions3d.GNYPsiEpsTest2020
import qualified Projects.Scalars3d.SingletScalar2020
import qualified Projects.Scalars3d.SingletScalarBlocks3d2020
import qualified Projects.Currents4d.JJJJ4d2020
import qualified Projects.Scalars3d.ONVec2021

main :: IO ()
main = hyperionBootstrapMain config $
  tryAllPrograms
  [ Projects.Scalars3d.IsingSigEpsTest2020.boundsProgram
  , Projects.Fermions3d.FourFermions3dTest2020.boundsProgram
  , Projects.Fermions3d.GNYTest2020.boundsProgram
  , Projects.Fermions3d.GNYPsiEpsTest2020.boundsProgram
  , Projects.Fermions3d.GNYSigPsiEpsTest2020.boundsProgram
  , Projects.Scalars3d.SingletScalar2020.boundsProgram
  , Projects.Scalars3d.SingletScalarBlocks3d2020.boundsProgram
  , Projects.Currents4d.JJJJ4d2020.boundsProgram
  , Projects.Scalars3d.ONVec2021.boundsProgram
  ]
